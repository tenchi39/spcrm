<br>	
<?php



if(isset($_REQUEST['contact_id']) and is_numeric($_REQUEST['contact_id']) and countMysqlItems('contacts', "WHERE contacts_id='".$_REQUEST['contact_id']."' LIMIT 1") == 1) {

	// Update likes
	if(isset($_REQUEST['action'])) {
		if($_REQUEST['action'] == 'update_likes') {
			mysqli_query($link, "UPDATE contacts SET contacts_likes='".mysqli_real_escape_string($link, $_REQUEST['likes'])."' WHERE contacts_id='".$_REQUEST['contact_id']."' LIMIT 1");
			echo mysqli_error($link);
		}
	}

	// Update dislikes
	if(isset($_REQUEST['action'])) {
		if($_REQUEST['action'] == 'update_dislikes') {
			mysqli_query($link, "UPDATE contacts SET contacts_dislikes='".mysqli_real_escape_string($link, $_REQUEST['dislikes'])."' WHERE contacts_id='".$_REQUEST['contact_id']."' LIMIT 1");
			echo mysqli_error($link);
		}
	}

	// Update notes
	if(isset($_REQUEST['action'])) {
		if($_REQUEST['action'] == 'update_notes') {
			mysqli_query($link, "UPDATE contacts SET contacts_notes='".mysqli_real_escape_string($link, $_REQUEST['notes'])."' WHERE contacts_id='".$_REQUEST['contact_id']."' LIMIT 1");
			echo mysqli_error($link);
		}
	}



	echo '<div class="grid-container">';
	$result = mysqli_query($link, "SELECT * FROM contacts WHERE contacts_id='".$_REQUEST['contact_id']."' LIMIT 1");
	$myrow = mysqli_fetch_assoc($result);


	echo '<div class="grid-x grid-margin-x">';
	// Photo, display name, occupation, company
  	echo '<div id="content-top" class="cell small-12 large-8">';

  	echo '<div class="grid-x grid-margin-x">';
  	echo '<div class="cell small-12">';
	$portrait = getFilename('data/portraits', $myrow['contacts_id'].'-1-');
	if($portrait != '') {
		echo '<img class="portrait" src="'.$portrait.'" />';
	} else {
		echo '<img class="portrait" src="img/placeholder-torso.gif" />';
	}
	echo '<h1>'.$myrow['contacts_name_display'];
	if($myrow['contacts_name_nickname'] != '') {
		echo ' <br class="show-for-small-only"><small>'.$myrow['contacts_name_nickname'].'</small>';
	}
	echo '</h1>';
	if($myrow['contacts_occupation'] != '') {
		echo '<h2>'.$myrow['contacts_occupation'].'</h2>';
	}
	if($myrow['contacts_company'] != '') {
		echo '<h3>'.$myrow['contacts_company'].'</h3>';
	}
  	echo '</div>';
  	echo '</div>';
  	echo '<br />';
	// Personal data
  	echo '<fieldset class="fieldset">';
	echo '<legend><i class="fi-book"></i> '.lng('personal_data').'</legend>';
	echo '<div class="grid-x grid-margin-x">';
	// No personal data
	if($myrow['contacts_email'] == '' and $myrow['contacts_phone'] == '' and $myrow['contacts_address'] == '' and $myrow['contacts_social'] == '') {
	  	echo '<div class="cell small-12">';
		echo '<p>'.lng('no_personal_data').'</p>';
	  	echo '</div>';
	}
	// Photo, occupation, company
  	// Email
  	if($myrow['contacts_email'] != '') {
	  	echo '<div class="cell small-12 medium-2 large-3">';
	  	echo '<p class="show-for-medium-only"><b>'.lng('email').':</b></p>';
	  	echo '<p class="hide-for-medium-only"><b>'.lng('email_address').':</b></p>';
	  	echo '</div>';
	  	echo '<div class="cell small-12 medium-10 large-9 ">';
	  	$email = explode(',', $myrow['contacts_email']);
	  	foreach($email as $value) {
	  		$value = trim($value);
			echo '<p><a href="mailto:'.$value.'">'.$value.'</a></p>';
		}
	  	echo '</div>';
	}
  	// Phone
  	if($myrow['contacts_phone'] != '') {
	  	echo '<div class="cell small-12 medium-2 large-3">';
	  	echo '<p class="show-for-medium-only"><b>'.lng('phone').':</b></p>';
	  	echo '<p class="hide-for-medium-only"><b>'.lng('phone_number').':</b></p>';
	  	echo '</div>';
	  	echo '<div class="cell small-12 medium-10 large-9 ">';
	  	$phone = explode(',', $myrow['contacts_phone']);
	  	foreach($phone as $value) {
	  		$value = trim($value);
			echo '<p><a href="tel:'.$value.'">'.$value.'</a></p>';
		}
	  	echo '</div>';
  	}
  	// Address
  	if($myrow['contacts_address'] != '') {
	  	echo '<div class="cell small-12 medium-2 large-3">';
	  	echo '<p><b>'.lng('address').':</b></p>';
	  	echo '</div>';
	  	echo '<div class="cell small-12 medium-10 large-9 ">';
	  	$address = explode("\n", $myrow['contacts_address']);
	  	foreach($address as $value) {
	  		$value = trim($value);
			echo '<p>'.$value.'</p>';
		}
	  	echo '</div>';
  	}
  	// Social
  	if($myrow['contacts_social'] != '') {
	  	echo '<div class="cell small-12 medium-2 large-3">';
	  	echo '<p class="show-for-medium-only"><b>'.lng('social').':</b></p>';
	  	echo '<p class="hide-for-medium-only"><b>'.lng('social_media').':</b></p>';
	  	echo '</div>';
	  	echo '<div class="cell small-12 medium-10 large-9 ">';

	  	$social = explode("\n", $myrow['contacts_social']);
	  	foreach($social as $value) {
	  		$value = trim($value);
	  		$value = explode(':', $value, 2);
			echo '<p><a href="'.$value[1].'" target="_blank">'.getSocialIcon($value[0]).$value[0].'</a></p>';
		}
	  	echo '</div>';
  	}



  	////
  	echo '</div>';
	echo '</fielset>';
  	echo '</div>';
  	// Likes, dislikes, notes
  	echo '<div class="cell small-12 large-4">';
  	echo '<div class="grid-x grid-margin-x" data-equalizer data-equalize-on="medium" id="test-eq">';
  	// Likes
  	echo '<div class="cell small-12 medium-6 large-12">';
  	echo '<fieldset class="fieldset" data-equalizer-watch>';
	echo '<legend><i class="fi-like"></i> '.lng('likes').'</legend>';

	echo '<div contenteditable="true" class="contenteditable" id="likes_content" onblur="updateField(\'contacts\', \'contacts_likes\', \''.$myrow['contacts_id'].'\', \'likes_content\');">';
	if($myrow['contacts_likes'] != '') {
		echo $myrow['contacts_likes'];
	} else {
		echo '...';
	}
	echo '</div>';
	echo '</fieldset>';
	echo '</div>';
  	// Dislikes
  	echo '<div class="cell small-12 medium-6 large-12">';
  	echo '<fieldset class="fieldset" data-equalizer-watch>';
	echo '<legend><i class="fi-dislike"></i> '.lng('dislikes').'</legend>';
	echo '<div contenteditable="true" class="contenteditable" id="dislikes_content" onblur="updateField(\'contacts\', \'contacts_dislikes\', \''.$myrow['contacts_id'].'\', \'dislikes_content\');">';
  	if($myrow['contacts_dislikes'] != '') {
  		echo $myrow['contacts_dislikes'];
	} else {
		echo '...';
	}
	echo '</div>';
	echo '</fieldset>';
	echo '</div>';
  	// Notes
  	echo '<div class="cell small-12 large-12">';
  	echo '<fieldset class="fieldset">';
	echo '<legend><i class="fi-clipboard-notes"></i> '.lng('notes').'</legend>';
	echo '<div contenteditable="true" class="contenteditable" id="notes_content" onblur="updateField(\'contacts\', \'contacts_notes\', \''.$myrow['contacts_id'].'\', \'notes_content\');">';
  	if($myrow['contacts_notes'] != '') {
  		echo $myrow['contacts_notes'];
	} else {
		echo '...';
	}
	echo '</div>';
	echo '</fieldset>';
  	echo '</div>';

  	echo '</div>';



	echo '<br />';
	echo '<br />';



	echo '<br />';
	echo '<br />';
	echo '[<a href="index.php?function=contact_form&amp;contact_id='.$myrow['contacts_id'].'">'.lng('edit').'</a>] ';
	echo '[<a href="index.php?function=contacts&action=delete_contact&amp;contact_id='.$myrow['contacts_id'].'" onclick="return confirm(\''.lng('are_you_sure').'\')">'.lng('delete').'</a>]';
	echo '</div>';
} else {
	include('lib/404.php');
}
