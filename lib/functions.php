<?php

function countMysqlItems($table, $where) {
	global $link;
	$result = mysqli_query($link, "SELECT ".$table."_id FROM ".$table." ".$where);
    $counter = mysqli_num_rows($result);
	return $counter;
}

function lng($id) {
	global $lng;
	return $lng[$id];
}

function humanReadableSize($size) {
	if(strlen($size) <= 9 && strlen($size) >= 7) {
		$size = number_format($size / 1048576,1);
		$size .= " MB";
	} elseif(strlen($size) >= 10) {
		$size = number_format($size / 1073741824,1);
		$size .= " GB";
	} else {
		$size = number_format($size / 1024,1);
		$size .= " KB";
	}
	return $size;
}

// Returns a file size limit in bytes based on the PHP upload_max_filesize and post_max_size
function file_upload_max_size() {
  static $max_size = -1;

  if ($max_size < 0) {
    // Start with post_max_size.
    $post_max_size = parse_size(ini_get('post_max_size'));
    if ($post_max_size > 0) {
      $max_size = $post_max_size;
    }

    // If upload_max_size is less, then reduce. Except if upload_max_size is
    // zero, which indicates no limit.
    $upload_max = parse_size(ini_get('upload_max_filesize'));
    if ($upload_max > 0 && $upload_max < $max_size) {
      $max_size = $upload_max;
    }
  }
  return $max_size;
}

function parse_size($size) {
  $unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
  $size = preg_replace('/[^0-9\.]/', '', $size); // Remove the non-numeric characters from the size.
  if ($unit) {
    // Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
    return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
  }
  else {
    return round($size);
  }
}

function getFilename($directory, $prefix) {
	$dir_handle = opendir($directory);
	while($file = readdir($dir_handle)) {
		if($file != '.' and $file != '..') {
			$pos = strpos($file, $prefix);
			if($pos === false) {
			} elseif($pos == 0) {
				$filename = $directory.'/'.$file;
				closedir($dir_handle);
				return $filename;
			}
		}
	}
	closedir($dir_handle);
}

function getFilenames($directory, $prefix) {
	$files = array();
	$dir_handle = opendir($directory);
	while($file = readdir($dir_handle)) {
		if($file != '.' and $file != '..') {
			if($prefix != '') {
				$pos = strpos($file, $prefix);
				if($pos === false) {
				} elseif($pos == 0)	{
					$files[] = $directory.'/'.$file;
				}
			} else {
				$files[] = $directory.'/'.$file;
			}
		}
	}
	closedir($dir_handle);
	return $files;
}

function getSocialIcon($name) {
	$name = strtolower($name);
	if($name == 'linkedin') {
		return '<i class="fi-social-linkedin"></i> ';
	} else if($name == 'github') {
		return '<i class="fi-social-github"></i> ';
	} else if($name == 'facebook' or $name == 'messenger') {
		return '<i class="fi-social-facebook"></i> ';
	} else if($name == 'skype') {
		return '<i class="fi-social-skype"></i> ';
	} else if($name == 'snapchat') {
		return '<i class="fi-social-snapchat"></i> ';
	} else if($name == 'spotify') {
		return '<i class="fi-social-spotify"></i> ';
	} else if($name == 'steam') {
		return '<i class="fi-social-steam"></i> ';
	} else if($name == 'twitter') {
		return '<i class="fi-social-twitter"></i> ';
	} else if($name == 'instagram') {
		return '<i class="fi-social-instagram"></i> ';
	} else if($name == 'pinterest') {
		return '<i class="fi-social-pinterest"></i> ';
	} else if($name == 'reddit') {
		return '<i class="fi-social-reddit"></i> ';
	} else if($name == 'tumblr') {
		return '<i class="fi-social-tumblr"></i> ';
	} else {
		return '<i class="fi-star"></i> ';
	}
}

function purgeTmp() {
	$time = time();
	$time -= 10800;
	unset($files);
	$files = getFilenames('data/tmp', '');
	if(!empty($files)) {
		foreach($files as $value) {
			if($value != 'data/tmp/.gitkeep') {
				if(filemtime($value) < $time) {
					unlink($value);
				}
			}
		}
	}
}