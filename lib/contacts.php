<div class="grid-container">

<?php
	if(isset($_REQUEST['action'])) {

		if($_REQUEST['action'] == 'delete_contact' and isset($_REQUEST['contact_id']) and is_numeric($_REQUEST['contact_id']) and countMysqlItems('contacts', "WHERE contacts_id='".$_REQUEST['contact_id']."' LIMIT 1") == 1) {
			// Get contact info
			$result = mysqli_query($link, "SELECT * FROM contacts WHERE contacts_id='".$_REQUEST['contact_id']."' LIMIT 1");
			$myrow = mysqli_fetch_assoc($result);

			// Delete contact
			mysqli_query($link, "UPDATE contacts SET contacts_deleted=1 WHERE contacts_id='".$_REQUEST['contact_id']."' LIMIT 1");
			echo mysqli_error($link);

			// Delete success message
			echo '<div class="callout success" data-closable>';
			echo '<h5>'.lng('contact_deleted').'</h5>';
			echo '<p><b>'. $myrow['contacts_name_display'].'</b> '.lng('has_been_deleted').'</p>';
			echo '<p><a href="index.php?function=contacts&amp;action=undo_delete&amp;contact_id='.$_REQUEST['contact_id'].'">'.lng('undo').'</a></p>';
			echo '<button class="close-button" aria-label="'.lng('dismiss_alert').'" type="button" data-close>';
			echo '<span aria-hidden="true">&times;</span>';
			echo '</button>';
			echo '</div>';
		}
		if($_REQUEST['action'] == 'undo_delete' and isset($_REQUEST['contact_id']) and is_numeric($_REQUEST['contact_id']) and countMysqlItems('contacts', "WHERE contacts_id='".$_REQUEST['contact_id']."' LIMIT 1") == 1) {
			// Get contact info
			$result = mysqli_query($link, "SELECT * FROM contacts WHERE contacts_id='".$_REQUEST['contact_id']."' LIMIT 1");
			$myrow = mysqli_fetch_assoc($result);

			// Delete contact
			mysqli_query($link, "UPDATE contacts SET contacts_deleted=0 WHERE contacts_id='".$_REQUEST['contact_id']."' LIMIT 1");
			echo mysqli_error($link);

			// Delete success message
			echo '<div class="callout success" data-closable>';
			echo '<h5>'.lng('contact_restored').'</h5>';
			echo '<p><a href="index.php?function=contact&amp;contact_id='.$myrow['contacts_id'].'">'. $myrow['contacts_name_display'].'</a> '.lng('has_been_restored').'</p>';
			echo '<button class="close-button" aria-label="'.lng('dismiss_alert').'" type="button" data-close>';
			echo '<span aria-hidden="true">&times;</span>';
			echo '</button>';
			echo '</div>';
		}
	}
?>


<a href="index.php?function=contact_form" class="float-right"><i class="fi-plus"></i> <?php echo lng('add_contact'); ?></a>
<h1><?php echo lng('contacts'); ?></h1>

<div class="grid-x grid-margin-x">
	<div class="cell small-12">

		<?php

			$first = true;
			foreach($abc as $letter) {
				if(countMysqlItems('contacts', "WHERE contacts_deleted=0 AND contacts_name_display LIKE '".$letter."%' COLLATE utf8_bin LIMIT 1") != 0) {
					if($first == true) {
						$first = false;
					} else {
						echo ' | ';
					}
					echo '<a onclick="scrollToId(\'h2-'.$letter.'\', 70);">'.$letter.'</a>';
				}
			}

		?>		
		<br>

		<?php

			$initial = '';

			$result = mysqli_query($link, "SELECT * FROM contacts WHERE contacts_deleted=0 ORDER BY contacts_name_display");
			while($myrow = mysqli_fetch_assoc($result)) {
				if(substr($myrow['contacts_name_display'], 0, 1) != $initial) {
					$initial = substr($myrow['contacts_name_display'], 0, 1);
					echo '<br />';
					echo '<h2 id="h2-'.$initial.'">'.$initial.'</h2>';
					echo '<hr />';
				}


				echo '<div class="grid-x align-middle clickable" onclick="window.location=\'index.php?function=contact&amp;contact_id='.$myrow['contacts_id'].'\';">';
				// First column: Portrait
  				echo '<div class="cell small-3 medium-2 large-1 text-center">';

					$portrait = getFilename('data/portraits', $myrow['contacts_id'].'-1-');
					if($portrait != '') {
						echo '<img class="portrait-small" src="'.$portrait.'" />';
					} else {
						echo '<img class="portrait-small" src="img/placeholder-torso.gif" />';
					}
  				echo '</div>';
  				// Second column: Name, position, company
  				echo '<div class="cell small-9 medium-5 large-5">';
				echo '<big><b>';
				echo $myrow['contacts_name_display'];
				if($myrow['contacts_name_nickname'] != '') {
					echo ' <small>('.$myrow['contacts_name_nickname'].')</small>';
				}
				echo '</b></big>';
				if($myrow['contacts_occupation'] != '') {
					echo '<br /><small>'.$myrow['contacts_occupation'].'</small>';
				}
				if($myrow['contacts_company'] != '') {
					echo '<br /><small>'.$myrow['contacts_company'].'</small>';
				}
  				echo '</div>';
  				// This is needed for small screens
  				echo '<div class="cell small-3 show-for-small-only">';
  				echo '</div>';
  				// Third column: Email, phone
  				echo '<div class="cell small-9 medium-5 large-6">';
				if($myrow['contacts_email'] != '') {
					$email = explode(',', $myrow['contacts_email']);
					$email = trim($email[0]);
					echo '<small><i class="fi-mail"></i> <a href="mailto:'.$email.'">'.$email.'</a></small><br />';
				}
				if($myrow['contacts_phone'] != '') {
					$phone = explode(',', $myrow['contacts_phone']);
					$phone = trim($phone[0]);
					echo '<small><i class="fi-telephone"></i> <a href="tel:'.$phone.'">'.$phone.'</a></small><br />';
				}

  				echo '</div>';
  				echo '</div>';
			}
		?>


	</div>
</div>

</div>