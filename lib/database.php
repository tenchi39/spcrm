<?php

if($db_port != '') {
	$db_host .=':'.$db_port;
};

if(!@mysqli_connect($db_host, $db_user, $db_password)) {
	die('Error connecting to database! Check db values in config.php');
	exit();
}

$link = mysqli_connect($db_host, $db_user, $db_password);

if(!@mysqli_select_db($link, $db_database)) {
	die('Error opening database table! Check $db_database in config.php');
	exit();
}

mysqli_set_charset($link, "utf8");