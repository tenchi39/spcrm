<div class="grid-container">

<?php

$max_file_size = humanReadableSize(file_upload_max_size());

// If the form has been submitted
$errors = false;
$errors_specific = array();
$errors_file[1] = false;

// File uploads

if(!isset($_REQUEST['uploaded_file_1'])) {
	$_REQUEST['uploaded_file_1'] = '';
}

if($demo == false) {
	if(isset($_REQUEST['delete_file_1']) and $_REQUEST['delete_file_1'] != '') {
		if(is_file('data/tmp/'.$_REQUEST['uploaded_file_1'])) {
			unlink('data/tmp/'.$_REQUEST['uploaded_file_1']);
			unset($_REQUEST['uploaded_file_1']);
			$errors = true;
		} else {
			$filename = getFilename('data/portraits', $_REQUEST['contact_id'].'-1-');
			if($filename != '') {
				unlink($filename);
				$errors = true;
			}
		}
	}
}

$files = array();

if($demo == false) {
	// If there are files being uploaded
	if(!empty($_FILES)) {
		$tmpdir = 'data/tmp/';
		if(!empty($_FILES['file1']) and $_FILES['file1']['name'] != '') {
			if($_FILES['file1']['error'] == 0) {
				$filename = '1-'.basename($_FILES['file1']['name']);
				$files[1] = $filename;
				move_uploaded_file($_FILES['file1']['tmp_name'], $tmpdir.$filename);
			} else {
				$errors = true;
				$errors_file[1] = true;
			}
		}
	}
}

if(isset($_REQUEST['uploaded_file_1']) and $_REQUEST['uploaded_file_1'] != '') {
	$files[1] = $_REQUEST['uploaded_file_1'];
}

if(isset($_REQUEST['submitted']) and $_REQUEST['submitted'] == true) {

	if($_REQUEST['contacts_name_display'] == '') {
		$errors = true;
		$errors_specific['contacts_name_display'] = true;
	}

	if($errors == true) {
		if(isset($errors_specific['contacts_name_display'])) {
		echo '<div class="callout alert">';
		echo '<h5>'.lng('missing_fields').'</h5>';
		echo '<small>';
		echo '<ul>';
		if(isset($errors_specific['contacts_name_display'])) {
			echo '<li>'.lng('display_name').' '.lng('is_required').'</li>';
		}
		echo '<ul>';
		echo '</small>';
		echo '</div>';
		}
	} else {
		// Updating a contact
		if(isset($_REQUEST['contact_id']) and is_numeric($_REQUEST['contact_id']) and countMysqlItems('contacts', "WHERE contacts_id='".$_REQUEST['contact_id']."' LIMIT 1") == 1) {
			
			mysqli_query($link, "UPDATE contacts SET
				contacts_name_surname='".mysqli_real_escape_string($link, $_REQUEST['contacts_name_surname'])."', 
				contacts_name_forename='".mysqli_real_escape_string($link, $_REQUEST['contacts_name_forename'])."', 
				contacts_name_nickname='".mysqli_real_escape_string($link, $_REQUEST['contacts_name_nickname'])."', 
				contacts_name_display='".mysqli_real_escape_string($link, $_REQUEST['contacts_name_display'])."', 
				contacts_occupation='".mysqli_real_escape_string($link, $_REQUEST['contacts_occupation'])."', 
				contacts_company='".mysqli_real_escape_string($link, $_REQUEST['contacts_company'])."', 
				contacts_email='".mysqli_real_escape_string($link, $_REQUEST['contacts_email'])."', 
				contacts_phone='".mysqli_real_escape_string($link, $_REQUEST['contacts_phone'])."', 
				contacts_address='".mysqli_real_escape_string($link, $_REQUEST['contacts_address'])."', 
				contacts_social='".mysqli_real_escape_string($link, $_REQUEST['contacts_social'])."' 
			WHERE contacts_id='".$_REQUEST['contact_id']."' LIMIT 1");
			echo mysqli_error($link);
			// Update success message
			echo '<div class="callout success">';
			echo '<h5>'.lng('contact_updated').'</h5>';
			echo '<p><a href="index.php?function=contact&amp;contact_id='.$_REQUEST['contact_id'].'">'.$_REQUEST['contacts_name_display'].'</a> '.lng('has_been_updated').'</p>';
			echo '<a class="button" href="index.php?function=contact&amp;contact_id='.$_REQUEST['contact_id'].'">'.lng('go_back').'</a>';
			echo '</div>';
			// Move the uploaded portrait from tmp
			if(isset($files[1]) and $files[1] != '') {
				copy('data/tmp/'.$files[1], 'data/portraits/'.$_REQUEST['contact_id'].'-'.$files[1]);
				unlink('data/tmp/'.$files[1]);
			}
			return;
		} else {
			// Inserting new contact
			mysqli_query($link, "INSERT INTO contacts (
				contacts_name_surname, 
				contacts_name_forename, 
				contacts_name_nickname, 
				contacts_name_display, 
				contacts_occupation, 
				contacts_company, 
				contacts_email, 
				contacts_phone, 
				contacts_address, 
				contacts_social
			) VALUES (
				'".mysqli_real_escape_string($link, $_REQUEST['contacts_name_surname'])."', 
				'".mysqli_real_escape_string($link, $_REQUEST['contacts_name_forename'])."', 
				'".mysqli_real_escape_string($link, $_REQUEST['contacts_name_nickname'])."', 
				'".mysqli_real_escape_string($link, $_REQUEST['contacts_name_display'])."', 
				'".mysqli_real_escape_string($link, $_REQUEST['contacts_occupation'])."', 
				'".mysqli_real_escape_string($link, $_REQUEST['contacts_company'])."', 
				'".mysqli_real_escape_string($link, $_REQUEST['contacts_email'])."', 
				'".mysqli_real_escape_string($link, $_REQUEST['contacts_phone'])."', 
				'".mysqli_real_escape_string($link, $_REQUEST['contacts_address'])."', 
				'".mysqli_real_escape_string($link, $_REQUEST['contacts_social'])."'
			)");
			echo mysqli_error($link);
			$inserted_contact_id = mysqli_insert_id($link);


			// Update success message
			echo '<div class="callout success">';
			echo '<h5>'.lng('contact_added').'</h5>';
			echo '<p><a href="index.php?function=contact&amp;contact_id='.$inserted_contact_id.'">'.$_REQUEST['contacts_name_display'].'</a> '.lng('has_been_added').'</p>';
			echo '<div class="button-group">';
			echo '<a class="button" href="index.php?function=contact&amp;contact_id='.$inserted_contact_id.'">'.lng('view').'</a>';
			echo '<a class="button" href="index.php?function=contact_form">'.lng('add_another').'</a>';
			echo '<a class="button" href="index.php?function=contacts">'.lng('go_back').'</a>';
			echo '</div>';
			echo '</div>';
			// Move the uploaded portrait from tmp
			if(isset($files[1]) and $files[1] != '') {
				copy('data/tmp/'.$files[1], 'data/portraits/'.$inserted_contact_id.'-'.$files[1]);
				unlink('data/tmp/'.$files[1]);
			}
			return;
		}
	}
}


// Form

echo '<form enctype="multipart/form-data" action="index.php?function=contact_form" method="POST">';

if(isset($_REQUEST['contact_id']) and is_numeric($_REQUEST['contact_id']) and countMysqlItems('contacts', "WHERE contacts_id='".$_REQUEST['contact_id']."' LIMIT 1") == 1 and !isset($_REQUEST['submitted'])) {

	$result = mysqli_query($link, "SELECT * FROM contacts WHERE contacts_id='".$_REQUEST['contact_id']."' LIMIT 1");
	$myrow = mysqli_fetch_assoc($result);
	$_REQUEST['contacts_name_surname'] = $myrow['contacts_name_surname'];
	$_REQUEST['contacts_name_forename'] = $myrow['contacts_name_forename'];
	$_REQUEST['contacts_name_nickname'] = $myrow['contacts_name_nickname'];
	$_REQUEST['contacts_name_display'] = $myrow['contacts_name_display'];
	$_REQUEST['contacts_occupation'] = $myrow['contacts_occupation'];
	$_REQUEST['contacts_company'] = $myrow['contacts_company'];
	$_REQUEST['contacts_email'] = $myrow['contacts_email'];
	$_REQUEST['contacts_phone'] = $myrow['contacts_phone'];
	$_REQUEST['contacts_address'] = $myrow['contacts_address'];
	$_REQUEST['contacts_social'] = $myrow['contacts_social'];
	echo '<h1>'.lng('edit_contact').'</h1>';
	echo '<input type="hidden" name="contact_id" value="'.$_REQUEST['contact_id'].'" />';
} else {
	echo '<h1>'.lng('add_contact').'</h1>';
	if(isset($_REQUEST['contact_id']) and is_numeric($_REQUEST['contact_id'])) {
		echo '<input type="hidden" name="contact_id" value="'.$_REQUEST['contact_id'].'" />';
	}
}

echo '<input type="hidden" name="submitted" value="true" />';
// Surname
echo '<label>';
echo lng('surname');
echo '<input name="contacts_name_surname" id="contacts_name_surname" type="text" value="';
if(isset($_REQUEST['contacts_name_surname'])) {
	echo $_REQUEST['contacts_name_surname'];
}
echo '">';
echo '</label>';
// Forename
echo '<label>';
echo lng('forename');
echo '<input name="contacts_name_forename" id="contacts_name_forename" type="text" value="';
if(isset($_REQUEST['contacts_name_forename'])) {
	echo $_REQUEST['contacts_name_forename'];
}
echo '">';
echo '</label>';
// Nickname
echo '<label>';
echo lng('nickname');
echo '<input name="contacts_name_nickname" id="contacts_name_nickname" type="text" value="';
if(isset($_REQUEST['contacts_name_nickname'])) {
	echo $_REQUEST['contacts_name_nickname'];
}
echo '">';
echo '</label>';
// Display name
echo '<label>';
echo lng('display_name').' <sup>*</sup>';
echo '<input name="contacts_name_display" id="contacts_name_display" type="text" value="';
if(isset($_REQUEST['contacts_name_display'])) {
	echo $_REQUEST['contacts_name_display'];
}
echo '">';
echo '</label>';
if(isset($errors_specific['contacts_name_display'])) {
	echo '<p class="help-text error" id="passwordHelpText">'.lng('this_field_is_required').'</p>';
}
// Occupation
echo '<label>';
echo lng('occupation');
echo '<input name="contacts_occupation" id="contacts_occupation" type="text" value="';
if(isset($_REQUEST['contacts_occupation'])) {
	echo $_REQUEST['contacts_occupation'];
}
echo '">';
echo '</label>';
// Company
echo '<label>';
echo lng('company');
echo '<input name="contacts_company" id="contacts_company" type="text" value="';
if(isset($_REQUEST['contacts_company'])) {
	echo $_REQUEST['contacts_company'];
}
echo '">';
echo '</label>';
// Email
echo '<label>';
echo lng('email');
echo '<input name="contacts_email" id="contacts_email" type="text" value="';
if(isset($_REQUEST['contacts_email'])) {
	echo $_REQUEST['contacts_email'];
}
echo '">';
echo '<p class="help-text">'.lng('you_can_add_multiple_items').'</p>';
echo '</label>';
// Phone
echo '<label>';
echo lng('phone');
echo '<input name="contacts_phone" id="contacts_phone" type="text" value="';
if(isset($_REQUEST['contacts_phone'])) {
	echo $_REQUEST['contacts_phone'];
}
echo '">';
echo '<p class="help-text">'.lng('you_can_add_multiple_items').'</p>';
echo '</label>';
// Address
echo '<label>';
echo lng('address');
echo '<textarea name="contacts_address" rows="5">';
if(isset($_REQUEST['contacts_address'])) {
	echo $_REQUEST['contacts_address'];
}
echo '</textarea>';
echo '<p class="help-text">'.lng('you_can_add_multiple_items_lines').'</p>';
echo '</label>';
// Social
echo '<label>';
echo lng('social_media');
echo '<textarea name="contacts_social" rows="5">';
if(isset($_REQUEST['contacts_social'])) {
	echo $_REQUEST['contacts_social'];
}
echo '</textarea>';
echo '<p class="help-text">'.lng('you_can_add_multiple_items_lines_social').'</p>';
echo '</label>';







///////////////

unset($filename);
$hide_file_upload = false;
if(isset($_REQUEST['contact_id']) and $_REQUEST['contact_id'] != '') {
	$filename = getFilename('data/portraits', $_REQUEST['contact_id'].'-1-');
	if($filename != '') {
		$hide_file_upload = true;
	}
}
if($hide_file_upload == false) {

	echo '<div class="small-12 cell">';
	echo '<label';
	if($errors_file[1] == true) {
		echo ' class="red"';
	}
	echo '>'.lng('portrait').'</label>';
	echo '<div class="grid-x">';
	if($demo == false) {
		if(!empty($files[1])) {
			echo '<div class="small-12 medium-10 cell">';
			echo '<input name="uploaded_file_1" type="text" value="'.$files[1].'" readonly="readonly" />';
			echo '</div>';
			echo '<div class="small-12 medium-2 cell">';
			echo '<input type="submit" name="delete_file_1" class="button button-margin-bottom-1rem alert expanded" value="'.lng('delete').'" onclick="return confirm(\'Are you sure?\')" />';
			echo '</div>';
		} else {
			echo '<div class="small-12 medium-10 cell">';
			echo '<input id="file1fakepath" type="text" readonly="readonly" />';
			echo '</div>';
			echo '<div class="small-12 medium-2 cell">';
			echo '<label for="file1" class="button expanded button-margin-bottom-1rem">'.lng('choose_file').'</label>';
			echo '<input type="file" id="file1" name="file1" onchange="document.getElementById(\'file1fakepath\').value=document.getElementById(\'file1\').value;" class="show-for-sr">';
			echo '</div>';
		}
		echo '<p class="help-text">'.lng('maximum_file_size').': '.$max_file_size.'</p>';
	} else {
		echo '<input type="text" value="'.lng('file_upload_is_disabled_in_demo_mode').'" disabled="disabled" />';
	}
	echo '</div>';
	echo '</div>';
} else {
	echo '<div class="small-12 cell">';
	echo '<label>'.lng('portrait').'</label>';
	echo '<div class="grid-x">';
	if($demo == false) {
		echo '<div class="small-12 medium-10 cell">';
		echo '<input name="already_uploaded_file_1" type="text" value="'.$filename.'" readonly="readonly" />';
		echo '</div>';
		echo '<div class="small-12 medium-2 cell">';
		echo '<input type="submit" name="delete_file_1" class="button button-margin-bottom-1rem alert expanded" value="'.lng('delete').'" onclick="return confirm(\''.lng('are_you_sure').'\')" />';
		echo '</div>';
	} else {
		echo '<div class="small-12 cell">';
		echo '<input type="text" value="'.lng('file_deletion_is_disabled_in_demo_mode').'" disabled="disabled" />';
		echo '</div>';
	}
	echo '<p class="help-text">'.lng('maximum_file_size').$max_file_size.'</p>';
	echo '</div>';
	echo '</div>';
}
























echo '<small><i>'.lng('fields_marked_with_an_asterisk').'</i></small>';

echo '<br />';
echo '<br />';
echo '<input type="submit" class="button" value="'.lng('save').'" />';

echo '</form>';

?>

</div>