<?php

// Initialize
$init_path = '';
include($init_path.'lib/init.php');

// Delete temporary files
purgeTmp();

$ajax_commands = array();

?><!doctype html>
<html class="no-js" lang="<?php echo $language; ?>">
<head>
	<base href="<?php echo $baseurl; ?>/" />
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>spCRM</title>
	<link rel="stylesheet" href="css/loading.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/foundation-sites@6.6.3/dist/css/foundation.min.css" integrity="sha256-ogmFxjqiTMnZhxCqVmcqTvjfe1Y/ec4WaRj/aQPvn+I=" crossorigin="anonymous">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/motion-ui.min.css">
	<link rel="stylesheet" href="css/foundation-icons.css">

	<link rel="apple-touch-icon" sizes="57x57" href="apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
	<link rel="manifest" href="manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
</head>
<body>
	<div class="loading" id="loading">Loading&#8230;</div>
	

	<!-- Sticky navigateion -->
	<div data-sticky-container>
		<div class="sticky" data-sticky data-sticky-on="small" data-margin-top="0">
			<!-- Mobile -->
			<div id="top-bar-mobile" class="grid-container show-for-small-only">
				<div class="grid-x grid-margin-x">
					<div class="cell small-3">
						<span class="logo-color-1">sp</span><span class="logo-color-2">CRM</span>
					</div>
					<div class="cell small-9">
						<div class="button-group float-right no-bottom-margin">
							<a class="button" href="index.php?function=contacts" title="<?php echo lng('contacts'); ?>"><i class="fi-torsos-all"></i></a>
							<a class="button" href="#" title="<?php echo lng('calendar'); ?>"><i class="fi-calendar"></i></a>
							<!-- <a class="button" href="#"><i class="fi-info"></i></a> -->
							<a class="button" href="#" title="<?php echo lng('search'); ?>"><i class="fi-magnifying-glass"></i></a>
							<a class="button" href="logout.php" title="<?php echo lng('log_out'); ?>"><i class="fi-lock"></i></a>
						</div>
					</div>
				</div>
			</div>

			<!-- Tablet and desktop -->
			<div id="top-bar-container" class="hide-for-small-only">
				<div class="grid-container">
					<div class="top-bar">
						<div class="top-bar-left">
							<ul class="menu" data-dropdown-menu>
								<li class="menu-text"><span class="logo-color-1">sp</span><span class="logo-color-2">CRM</span></li>
								<li><a href="index.php?function=contacts"><?php echo lng('contacts'); ?></a></li>
								<li><a href="#"><?php echo lng('calendar'); ?></a></li>
								<li><a href="#" class="hide-for-large-only"><?php echo lng('search'); ?></a></li>
								<li><a href="logout.php" class="hide-for-large-only"><?php echo lng('log_out'); ?></a></li>
							</ul>
						</div>
						<div class="top-bar-right show-for-large">
							<ul class="menu">
								<li><input type="search"></li>
								<li><button type="button" class="button"><?php echo lng('search'); ?></button></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<br />
	<?php

		$available_functions = array('contact', 'contacts', 'contact_form');

		if(!isset($_REQUEST['function'])) {
			$function = 'contacts';
		} else {
			if(in_array($_REQUEST['function'], $available_functions)) {
				$function = $_REQUEST['function'];
			} else {
				$function = '404';
			}
		}
		include('lib/'.$function.'.php');

	?>





	<script src="js/functions.js"></script>
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/foundation-sites@6.6.3/dist/js/foundation.min.js" integrity="sha256-pRF3zifJRA9jXGv++b06qwtSqX1byFQOLjqa2PTEb2o=" crossorigin="anonymous"></script>
	<script>
		
		<?php echo 'var ajaxUrlBase = \''.$baseurl."';\n"; ?>

		$(document).ready(function() {
			$(document).foundation();

			<?php

				//echo $baseurl;

				foreach ($ajax_commands as $key => $value) {
					echo $value;
				}
			?>

		});

		$(window).on("load", function() {
			$( "#loading" ).fadeOut( "slow", function() {
			});			
		});

	</script>
</body>
</html>
