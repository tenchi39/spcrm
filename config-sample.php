<?php

/** MySQL hostname */
$db_host = '';

/** MySQL port (optional) - For MySQL the default port is 3306, for MariaDB it is 3307 */
$db_port = '';

/** The name of the database for W2W */
$db_database = '';

/** MySQL database username */
$db_user = '';

/** MySQL database password */
$db_password = '';

/** Keep it on false if you are not actively debugging or modifying the code */
$development_mode = true;

/** Keep it on false for personal use as it disabled database operations **/
$demo = false;

/** Language */
$language = 'en';

/** ABC for used for initial letter navigation on the contacts page */

//$abc = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z");

$abc = array("A", "Á", "B", "C", "D", "E", "É", "F", "G", "H", "I", "Í", "J", "K", "L", "M", "N", "O", "Ó", "Ö", "Ő", "P", "Q", "R", "S", "T", "U", "Ú", "Ü", "Ű", "V", "W", "X", "Y", "Z");