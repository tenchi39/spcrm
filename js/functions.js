function scrollToId(id, scroll_offset) {
	$('html,body').animate({scrollTop: $("#"+id).offset().top - scroll_offset},'slow');
}

function removeClass(id, name) {
	$('#' + id).removeClass(name);
}

function addClass(id, name) {
	$('#' + id).addClass(name);
}

function addValue(id, value) {
	var content = $('#' + id).val();
	if(content == '') {
		content += value;	
	} else {
		content += ', ' + value;
	}
	$('#' + id).val(content);
}

function updateField(table, column, id, field) {
	var value = $('#' + field).html();
    $.ajax({
        type: 'post',
        url:  ajaxUrlBase + '/lib/ajax_update_field.php?table=' + table + '&column=' + column + '&id=' + id,
        data: 'value=' + value
    });
}